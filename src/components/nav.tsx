import Link from "next/link";
import { Center, Spacer, Text } from "@chakra-ui/react";
import { CardanoWallet } from "@meshsdk/react";

export default function Nav() {
  return (
    <Center
      w="100%"
      p="5"
      bg="#232323"
      color="white"
    >
      <Text>
        <Link href="/">
          Home
        </Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/projects">Projects</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/dashboard">Dashboard</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/contributors">Contributors</Link>
      </Text>
      <Spacer />
      <Text>
        <Link href="/distribute">Distribute</Link>
      </Text>
      <Spacer />

      <CardanoWallet />
    </Center>
  );
}
