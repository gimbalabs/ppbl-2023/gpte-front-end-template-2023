import { useQuery, gql } from "@apollo/client";

import {
  Box,
  Heading,
  Text,
  Center,
  Spinner,
} from "@chakra-ui/react";


// This component is a reference demo for for building specific data components

// What are the benefits of using one big query like this?
// What are the drawbacks?
const ESCROW_QUERY = gql`
  query ListOfCommitments($metadatakey: String!, $address: String!) {
    transactions(
      where: {
        _and: [
          { metadata: { key: { _eq: $metadatakey } } }
          {
            _or: [
              { outputs: { address: { _eq: $address } } }
              { inputs: { address: { _eq: $address } } }
            ]
          }
        ]
      }
    ) {
      hash
      includedAt
      metadata {
        key
        value
      }
      inputs {
        txHash
        sourceTxIndex
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
      outputs {
        txHash
        index
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
    }
  }
`;

type Props = {
  metadataKey: string;
  address: string;
};

const TaskStatusList: React.FC<Props> = ({ metadataKey, address }) => {
  // You can import escrow or treasury from /cardano/plutus
  // and use component-level variables any time, depending on your use case, ie:
  // const escrowAddress = escrow.address;
  // const contributorToken = treasury.accessTokenPolicyId
  // const projectToken = treasury.projectTokenAssetId

  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      metadatakey: metadataKey,
      address: address,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }


  return (
    <>
      <Box mt="5" p="5" border="1px" fontSize="xs">
        <Text py="3" fontSize="lg">For now, raw data:</Text>
        <pre>{JSON.stringify(data,null,2)}</pre>
      </Box>
    </>
  );
};

export default TaskStatusList;
