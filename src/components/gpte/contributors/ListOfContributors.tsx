import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text, Center, Spinner, Grid, OrderedList, ListItem } from "@chakra-ui/react";

import { contributorReferenceAddress, projectAsset, treasury } from "gpte-config";
import { GraphQLToken, GraphQLUTxO } from "../../../types";
import { CONTRIBUTOR_TOKEN_QUERY } from "../../../../src/queries/contributorQueries";
import { hexToString } from "../../../../src/utils";

export default function ListOfCommitments() {
  const { data, loading, error } = useQuery(CONTRIBUTOR_TOKEN_QUERY, {
    variables: {
      contractAddress: contributorReferenceAddress,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box my="5" p="5" bg="#343434">
      <Heading color="white">List of Contributors</Heading>
      <OrderedList py="3">
        <ListItem>Query Contributor Reference Address</ListItem>
        <ListItem>
          Format these results, depending on use case. What is the best way to demonstrate? Use PPBL2023 as one
          example.
        </ListItem>
      </OrderedList>
      {data.utxos.map((utxo: any) => (
        <Box bg="white" color="black" p="3" w="60%">
          <Text>Contributor: {hexToString(utxo.tokens[0].asset.assetName)}</Text>
          <Text>Datum: {JSON.stringify(utxo.datum.value.fields)}</Text>
        </Box>
      ))}
      <Text py="3" w="50%">In this example, the Contributor Datum contains a lucky number that PPBL students will learn to change as an initial Plutus exploration (or Plutus Puzzle!?), followed by a list of Completed Projects - in this case, Module Names</Text>
    </Box>
  );
}
