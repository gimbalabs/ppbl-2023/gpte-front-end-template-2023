import { escrowAddress, metadataKey } from "gpte-config";
import { Box, Heading, Text } from "@chakra-ui/react";
import { CardanoWallet, useWallet } from "@meshsdk/react";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
import ListOfCommitments from "../../../src/components/gpte/escrow/ListOfCommitments";

export default function DashboardPage() {
  const { connected, wallet } = useWallet();
  const [assets, setAssets] = useState<null | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  return (
    <>
      <Head>
        <title>GPTE Starter Template</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Box>
        <Heading>Dashboard</Heading>
      </Box>
      <ListOfCommitments metadataKey={metadataKey} address={escrowAddress} />
    </>
  );
}